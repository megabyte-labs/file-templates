FROM alpine:3

ENV container docker

RUN apk --no-cache add \
      nodejs~=14.18.1-r0 \
      npm~=7 \
  && npm install -g \
      @mblabs/prettier-config@latest \
      prettier@latest

WORKDIR /work
ENTRYPOINT ["prettier"]
CMD ["--version"]
